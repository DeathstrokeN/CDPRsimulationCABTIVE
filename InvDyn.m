function [t, W] = InvDyn(A, B, X, Xdot, X2dot, m, CoM, I)


W = WrenchMatrix(A, B); %-J'
[M, c, g] = DynamicMCG(m, I, CoM, Xdot);

%Mx2 + G = Wt
% W*t - M*X2dot - C*xdot - G = 0
% W*t = M*X2dot + c + G
Aeq = W;
beq = M*X2dot + c + g;
%Quadraticprog
H = ones(8);
f = zeros(1,8);
t = quadprog(H, f, [], [], Aeq, beq, 10*ones(8,1), 2000*ones(8,1));

%Must return vector of tension force
end
