#include <iostream>
#include <vector>

static double OFFSET_Z = 0.2;
static double pi = 3.14159265359;

static double Bp[3][8] = {
    {-0.2492, -0.2483, -0.2488, -0.1909, 0.2483, 0.1887, 0.2503, 0.2482},
    {0.2021, 0.2021, -0.2021, -0.2743, -0.2021, -0.2749, 0.2019, 0.2021},
    {0.1291, -0.2112, -0.2876, 0.2230, 0.2112, -0.2987, -0.2090, 0.1291}
};

static double Ai[3][8] =
{
-4.18012386167155,  -4.63029429558224,  -4.56238816887314,  -4.10392975470496,  4.10092060599512,  4.55525017351813,   4.55485066914982,  4.09649764452530,
-1.82310484386710,  -1.36543093757600,   1.32163248899797,   1.76558665087905,   1.78690397515010,   1.33450739925370,  -1.33940997327257,  -1.78941411953632,
2.88812529819398 - OFFSET_Z,   2.88681249171604 - OFFSET_Z,   2.89930447812487 - OFFSET_Z,   2.89863326833304 - OFFSET_Z,   2.90409783215508 - OFFSET_Z,   2.89954893233160 - OFFSET_Z,   2.89170331212063 - OFFSET_Z,   2.88884620949064 - OFFSET_Z
};



// Function to calculate 2D rotation matrix about the z-axis
void Rz(double theta, double rotation[3][3]) {
    rotation[0][0] = cos(theta);
    rotation[0][1] = -sin(theta);
    rotation[0][2] = 0;
    rotation[1][0] = sin(theta);
    rotation[1][1] = cos(theta);
    rotation[1][2] = 0;
    rotation[2][0] = 0;
    rotation[2][1] = 0;
    rotation[2][2] = 1;
}

// Rotation matrix of the robot

void Rot(double X[6], double rotation[3][3]) {
    double roll = X[3];
    double pitch = X[4];
    double yaw = X[5];

    double Rx[3][3] = { {1, 0, 0},
                       {0, cos(roll), -sin(roll)},
                       {0, sin(roll), cos(roll)} };

    double Ry[3][3] = { {cos(pitch), 0, sin(pitch)},
                       {0, 1, 0},
                       {-sin(pitch), 0, cos(pitch)} };

    double Rz[3][3] = { {cos(yaw), -sin(yaw), 0},
                       {sin(yaw), cos(yaw), 0},
                       {0, 0, 1} };

    // Multiply the rotation matrices Rz * Ry * Rx
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            rotation[i][j] = 0;
            for (int k = 0; k < 3; ++k) {
                rotation[i][j] += Rz[i][k] * Ry[k][j];
            }
        }
    }
}


// Function to calculate the real cable output in the pulley
void RealA(double A[3][8], double Ai[3][8], double B[3][8], double r_p) {
    // 1st to simplify the drawing, we traslate the origin of each pulley frame to the pulley center
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 8; ++j) {
            B[i][j] -= Ai[i][j];
        }
    }

    // Calculate the angles of pulley direction
    double angle[8];
    for (int i = 0; i < 8; ++i) {
        angle[i] = atan2(B[1][i], B[0][i]);
    }

    // Transform B in pulley plane
    for (int i = 0; i < 8; ++i) {
        double temp_x = B[0][i];
        B[0][i] = cos(-angle[i]) * temp_x - sin(-angle[i]) * B[1][i];
        B[1][i] = sin(-angle[i]) * temp_x + cos(-angle[i]) * B[1][i];
    }

    // Calculate A coordinates using Nguyen's method
    for (int i = 0; i < 8; ++i) {
        double z;
        if (i == 0 || i == 3 || i == 4 || i == 7) {
            z = (B[2][i] * r_p * r_p + r_p * std::abs(B[0][i] - r_p) * sqrt(B[2][i] * B[2][i] + (B[0][i] - r_p) * (B[0][i] - r_p) - r_p * r_p)) /
                (B[2][i] * B[2][i] + (B[0][i] - r_p) * (B[0][i] - r_p));
        }
        else {
            z = (2 * B[2][i] * r_p * r_p - r_p * std::abs(B[0][i] - r_p) * sqrt(B[2][i] * B[2][i] + (B[0][i] - r_p) * (B[0][i] - r_p) - r_p * r_p)) /
                (B[2][i] * B[2][i] + (B[0][i] - r_p) * (B[0][i] - r_p));
        }

        A[0][i] = r_p + sqrt(r_p * r_p - z * z);
        A[2][i] = z;
        A[1][i] = 0;
    }

    // Transform A back to the original frame
    for (int i = 0; i < 8; ++i) {
        double temp_x = A[0][i];
        A[0][i] = cos(angle[i]) * temp_x - sin(angle[i]) * A[1][i] + Ai[0][i];
        A[1][i] = sin(angle[i]) * temp_x + cos(angle[i]) * A[1][i] + Ai[1][i];
        A[2][i] = A[2][i] + Ai[2][i];
    }
}

int main() {
    // Example usage
    double A[3][8], B[3][8], r_p = 0.04, rotMat[3][3];
    double X_i[6] = { 0, 0, 1.5, 0, 0, pi/4 };


    //MGI to calculate B in Base frame
    Rot(X_i, rotMat);  // Rotates using roll, pitch, yaw from X[3] to X[5]
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 8; ++j) {
            B[i][j] = rotMat[i][0] * Bp[0][j] + rotMat[i][1] * Bp[1][j] + rotMat[i][2] * Bp[2][j] + X_i[i];
        }
    }


  
    // Call the RealA function
    RealA(A, Ai, B, r_p);

    // Display the result (replace with your own output formatting)
    //std::cout << "A = ";
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 8; ++j) {
            std::cout << A[i][j] << " ";
        }
        std::cout << std::endl;
    }

    return 0;
}
