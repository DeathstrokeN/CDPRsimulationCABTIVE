// ConsoleApplication1.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>

double atan2_custom(double y, double x) {
    // Handle special cases to avoid division by zero
    double M_PI = 3.14;
    if (x > 0) {
        return std::atan(y / x);
    }
    else if (x < 0) {
        if (y >= 0) {
            return std::atan(y / x) + M_PI;
        }
        else {
            return std::atan(y / x) - M_PI;
        }
    }
    else {
        if (y > 0) {
            return M_PI / 2;
        }
        else if (y < 0) {
            return -M_PI / 2;
        }
        else {
            return 0; // Undefined, return 0 in this case
        }
    }
}

int main() {
    double x = +1;
    double y = -1.0;

    double angle = atan2_custom(y, x);

    std::cout << "L'angle est : " << angle << " radians." << std::endl;
    std::cout << "L'angle est : " << angle*180/3.14 << " degrees." << std::endl;

    return 0;
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
