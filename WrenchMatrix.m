function W = WrenchMatrix(A, B)
%Must have A considering pulley geometry

%u, bi = B(:,i)
up      = A - B;
u       = nan(3,8);
for i = 1:8
    u(:,i) = up(:,i)/norm(up(:,i));
end

W = nan(6,8);

for i = 1:8   
    W(:,i)  = [u(:,i); cross(u(:,i), B(:,i))];
end

end