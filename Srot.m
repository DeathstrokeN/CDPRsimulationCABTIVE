function S = Srot(w)

S = [cos(w(3))*cos(w(2))    -sin(w(3))  0;
     sin(w(3))*cos(w(2))    cos(w(3))   0;
     -sin(w(3))             0           1];


end

