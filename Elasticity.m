function li_0 = Elasticity(A, B, tau)

%linear spring
%first calculate li, const is cable length between pulley and winch
const = 3; %3m hauteur de la base
li = nan(8, 1);
for i=1:8
    li(i) = sqrt((B(1,i) - A(1,i))^2 + (B(2,i) - A(2,i))^2 + (B(3,i) - A(3,i))^2) + const;
end

Ac = 0;
Ec = 0;

li_0(i)= nan(8,1);
for i=1:8
    li_0(i) = (Ac * Ec * li(i))/(tau(i) + Ac * Ec);
end


%output li_0 is translated to a motor position to set a cable length (coil and
%uncoil) considering elasticity

end

