function truncatedSignal = truncateSignal(inputSignal)
    strSignal = num2str(inputSignal, '%.9f'); % Convert to string with sufficient precision
    decimalPointIndex = strfind(strSignal, '.'); % Find the decimal point
    if ~isempty(decimalPointIndex)
        truncatedStrSignal = strSignal(1:(decimalPointIndex + 6)); % Keep up to six digits after decimal
        truncatedSignal = str2double(truncatedStrSignal); % Convert back to number
    else
        truncatedSignal = inputSignal; % No decimal point, return original signal
    end
end
