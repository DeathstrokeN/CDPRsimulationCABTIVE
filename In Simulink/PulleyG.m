function  PulleyG(Ai, angle, rp)


%A = Rz*Ai;

%%
%Draw pullies
%rp = 0.05; %pulley radius
circle = linspace(0, 2*pi, 100); %circle
signe = 1;

for i=1:8
    theta = angle(i);
    % Calculate the x and y coordinates of the points on the circle
    x = rp * sin(circle);
    y = zeros(1, length(circle));
    %y = A(2,i)*ones(length(circle), 1);
    z = rp * cos(circle);
    Pul = Rz(theta)*[x; y; z] + Ai(:,i) + signe * Rz(theta)*[rp;0;0];
    plot3(Pul(1,:), Pul(2,:), Pul(3,:), 'g', 'LineWidth', 3);
end



end