function S = MatrixS(Vec3d)

S = [0         -Vec3d(3)     Vec3d(2);
     Vec3d(3)   0          -Vec3d(1);
    -Vec3d(2)   Vec3d(1)     0];

end
