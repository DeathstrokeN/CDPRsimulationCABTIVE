function S = Srot_dot(w, wdot)


S = [-wdot(3)*sin(w(3))*cos(w(2))-wdot(2)*cos(w(3))*sin(w(2))   -wdot(3)*cos(w(3))   0;
      wdot(3)*cos(w(3))*cos(w(2))-wdot(2)*sin(w(3))*sin(w(2))   -wdot(3)*sin(w(3))  0;
     -wdot(2)*cos(w(2))  0   0];

end

