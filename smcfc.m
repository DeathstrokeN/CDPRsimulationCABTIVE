function h = smcfc(si)
delta = 1e-3*ones(6,1);

if si > delta
    h = 1;
elseif si <= delta 
    h = si / delta;
elseif si >= -delta
    h = si / delta;
else
    h = -1;
end

end

