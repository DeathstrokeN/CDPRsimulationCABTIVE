function [Xway, Xwaydot, Xway2dot] = TrajGen(tf, ts, Xi, Xf)

N = round(tf/ts);%number of simulation points tf/ts, 
Xway = nan(6,N);



for t=1:N
    r = polynomr5(tf, t*ts);
    Xway(:,t) = Xi + r*eye(6)*(Xf-Xi);
end

Xwaydot = zeros(6,N);
for t=2:N-1
    Xwaydot(:,t) = (Xway(:,t+1) - Xway(:,t))/ts;
    Xwaydot(4:6,t) = Srot(Xway(4:6,t))*Xwaydot(4:6,t);
end

Xway2dot = zeros(6,N);
for t=1:N-1
    Xway2dot(:,t) = (Xwaydot(:,t+1) - Xwaydot(:,t))/ts;
    Xway2dot(4:6,t) = Srot_dot(Xway(4:6,t), Xwaydot(4:6,t))*Xwaydot(4:6,t) + Srot(Xway(4:6,t))*Xway2dot(4:6,t);
end

%%
%plot
plotTrajectory(Xway, N, ts, tf, Xf)
plotTrajectory(Xwaydot, N, ts, tf, 0*Xf)
plotTrajectory(Xway2dot, N, ts, tf, 0*Xf)

end
