function [M, c, g] = DynamicMCG(m, I, CoM, xdot)

v2 = xdot(4:6); %omega

%CoGiRo article
%Motion of rigid body for point not coinciding with the MP CoM

M = nan(6,6);

M = [m*eye(3)           -m*MatrixS(CoM);
     m*MatrixS(CoM)      I];

c = [m*MatrixS(v2) * MatrixS(v2) * CoM;
     MatrixS(v2) * (I*eye(3) + m * MatrixS(v2) * MatrixS(v2)) *v2];

fg = [0  0  -m*9.8]';
g =  [fg; cross(fg, CoM)];

end

