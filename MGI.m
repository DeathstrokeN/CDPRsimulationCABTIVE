function [B, A, angle] = MGI(Bp, X, Ai, rp)

    %l = zeros(8);
    
    B = Rot(X(4:6))*Bp +  X(1:3); %B in base frame
    %Cables lenghts

    %for i=1:8
     %   l(i) = sqrt((B(1,i) - A(1,i))^2) + sqrt((B(2,i) - A(2,i))^2) + sqrt((B(3,i) - A(3,i))^2);
    %end

    [A, angle] = RealA(Ai, B, rp);

end