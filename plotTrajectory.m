function plotTrajectory(X, N, ts, tf, Xf)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

t=(0:ts:(tf-ts));
ref = Xf*ones(1,N);
figure('Name','Trajectory generation');
subplot(311);
plot(t,X(1,:),t, ref(1,:),'r', 'LineWidth',2)
legend('p_{xd}','p_{xf}')
xlabel('Time (s)')
ylabel('x(m)')
title('Trajectory position')
%axis equal
grid
subplot(312);
plot(t,X(2,:),t,ref(2,:),'r', 'LineWidth',2)
legend('p_{yd}','p_{yf}')
xlabel('Time (s)')
ylabel('y(m)')
%axis([0 N 0 1])
grid
subplot(313);
plot(t,X(3,:),t,ref(3,:),'r', 'LineWidth',2)
legend('p_{zd}','p_{zf}')
xlabel('Time (s)')
ylabel('z (m)')
%axis([0 N -pi pi])
grid

%orientation
figure('Name','Trajectory generation');
subplot(311);
plot(t,X(4,:),t, ref(4,:),'r', 'LineWidth',2)
legend('\alpha_d','\alpha_f')
xlabel('Time (s)')
ylabel('\alpha (rad)')
title('Trajectory orientation')
%axis equal
grid
subplot(312);
plot(t,X(5,:),t,ref(5,:),'r', 'LineWidth',2)
legend('\beta_d','\beta_f')
xlabel('Time (s)')
ylabel('\beta (rad)')
%axis([0 N 0 1])
grid
subplot(313);
plot(t,X(6,:), t,ref(6,:),'r', 'LineWidth',2)
legend('\gamma_d','\gamma_f')
xlabel('Time (s)')
ylabel('\gamma (rad)')
%axis([0 N -pi pi])
grid

end

