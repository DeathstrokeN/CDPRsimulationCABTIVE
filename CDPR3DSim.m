clc;clear all; close all;

tf = 10; %10s
ts = 0.001; %sampling time 10ms
N = round(tf/ts);
mass = 25.73; %kg
CoM = [8.64, -0.03, 188.53]' * 1e-3;%centre geom
%I is skew symmetic??
I = [2176870526.32      -69899831.59       -11268902.91;
    -69899831.59         2389077810.19     -1000235.91;
    -11268902.91        -1000235.91         1889912107.76] * 1e-9;%Inertia kg*m²

rp = .04; %pulley radius (80mm de diamètre)
Z_OFFSET_FIXED_FRAME = 0.2;

%%
%Drawing and attachment points 

% static double dimA[DIM_REPERE_ROBOT][NBR_AXE]=
% {
% -4.18012386167155,	-3.845507759387148,	-3.791318219896986,	-4.10392975470496,	4.10092060599512,	3.779653061521267,	3.780407488047897,	4.09649764452530,
%  -1.82310484386710,	 -1.367837380000738,	1.324747987699964,	1.76558665087905,	1.78690397515010,	 1.334595543377403,	-1.336999857000712,	 -1.78941411953632,
% 2.88812529819398-Z_OFFSET_FIXED_FRAME,	0.119457136996449-Z_OFFSET_FIXED_FRAME,	 0.127153685309803-Z_OFFSET_FIXED_FRAME,	2.89863326833304-Z_OFFSET_FIXED_FRAME,	2.90409783215508-Z_OFFSET_FIXED_FRAME,	0.128242662629527-Z_OFFSET_FIXED_FRAME,	0.123602457100741-Z_OFFSET_FIXED_FRAME,	2.88884620949064-Z_OFFSET_FIXED_FRAME
% };
% 
% static double dimB[DIM_REPERE_ROBOT][NBR_AXE]=
% {
% -0.2492,   -0.2483,   -0.2488,   -0.1909,    0.2483,    0.1887,    0.2503,    0.2482,
%  0.2021,    0.2021,   -0.2021,   -0.2743,   -0.2021,   -0.2749,    0.2019,    0.2021,
%  0.1291,   -0.2112,   -0.2876,    0.2230,    0.2112,   -0.2987,   -0.2090,    0.1291
% };

Bp = [-0.2492,   -0.2483,   -0.2488,   -0.1909,    0.2483,    0.1887,    0.2503,    0.2482;
       0.2021,    0.2021,   -0.2021,   -0.2743,   -0.2021,   -0.2749,    0.2019,    0.2021;
       0.1291,   -0.2112,   -0.2876,    0.2230,    0.2112,   -0.2987,   -0.2090,    0.1291];

Ai = [-4.18012386167155,	-3.845507759387148,	-3.791318219896986,	-4.10392975470496,	4.10092060599512,	3.779653061521267,	3.780407488047897,	4.09649764452530;
      -1.82310484386710,	 -1.367837380000738,	1.324747987699964,	1.76558665087905,	1.78690397515010,	 1.334595543377403,	-1.336999857000712,	 -1.78941411953632;
       2.88812529819398-Z_OFFSET_FIXED_FRAME,	0.119457136996449-Z_OFFSET_FIXED_FRAME,	 0.127153685309803-Z_OFFSET_FIXED_FRAME,	2.89863326833304-Z_OFFSET_FIXED_FRAME,	2.90409783215508-Z_OFFSET_FIXED_FRAME,	0.128242662629527-Z_OFFSET_FIXED_FRAME,	0.123602457100741-Z_OFFSET_FIXED_FRAME,	2.88884620949064-Z_OFFSET_FIXED_FRAME];


Xi = [0.2, 0.0, 1.5, 0, 0, 0]'; %initial pose in base frame 
Xf = [0.8, 0.0, 1.8, pi/6, 0, pi/4]'; %final pose in base frame
Xtest = [0.5, 0.2, 1.8, 0, pi/4, 0]';

% Bp = [.05   .05 -.05 -.05    .05  .05 -.05  -.05;
%       -.05  .05  .05 -.05   -.05  .05  .05  -.05;
%       .05   .05  .05  .05   -.05 -.05 -.05  -.05]; %attachment points in platform frame
% 
% 
% Ai = [1    0    0    1    1    0     0    1;
%       1    1    0    0    1    1     0    0;
%       1    1    1    1   -0    0    -0   -0]; %drawing "points" (Ps in the next function)

%%
[B, A, angle] = MGI(Bp, Xi, Ai, rp);

%%
%Inverse dynamic : static case
xdot  = zeros(6,1);
xdot2 = zeros(6,1);
ttpast = InvDyn(A, B, Xi, xdot, xdot2, mass, CoM, I);
[M, c, g] = DynamicMCG(mass, I, CoM, xdot);

Draw(Ai, A, B, Xi, angle, rp)

%%
% Design of a control LOOP
%%%Trajectory generation
[Xway, Xwaydot, Xway2dot] = TrajGen(tf, ts, Xi, Xf);
Xpast = Xi;
simdata_t = zeros(8,N);
simdata_X = zeros(6,N);
t_vec = linspace(0,tf,N);

P = 5e3*eye(6);%to compensate the error quickly
K_I = 1e1*eye(6);%to insure a null steady state error
D = 12e3*eye(6);%under 12e3 (with model mismatch 0.95*g) ==> important vibrations

error = zeros(6,1);
errorpast = zeros(6,1);
error_int = zeros(6,1);

for t=2:N-1
    ff          = M*Xway2dot(:,t) + 0.9*g;%
    fd = P * error + D * (error - errorpast) + K_I * error_int + ff;
    % W*t = M*X2dot + c + G = fd
    W = WrenchMatrix(A, B);
    Aeq = real(W);
    beq = fd;
    %Quadraticprog
    H = 8*ones(8);
    f = zeros(1,8);
    tt = quadprog(H, f, [], [], Aeq, beq, 10*ones(8,1), 2000*ones(8,1));
    [M, c, g] = DynamicMCG(mass, I, CoM, xdot);
    c
    x2dot = inv(M)*(W*tt - c - g);

    %plant with ode45
    tspan       = [t_vec(t-1) t_vec(t)];
    f           = @(t,y) [y(7:12);x2dot];
    ypast       = [Xpast; xdot];
    [~,y]       = ode45(f, tspan, ypast);
    y           = y(end,:);
    X           = y(1:6)';
    xdot        = y(7:12)';


    [B, A, angle] = MGI(Bp, X, Ai, rp);
    if mod(t,5) == 0
        Draw(Ai, A, B, X, angle, rp)
    end
    drawnow

    %storing data
    simdata_t(:,t-1) = ttpast;%cable tensions
    simdata_X(:,t) = X;

    %variables for new iteration
    errorpast = error;
    error  = Xway(:, t) - X;
    error_int   = error + error_int;

    Xpast  = X;
    ttpast = tt;

end

t=1:N-1;
figure('Name','system output');
plot(t,Xway(:,1:N-1),'k',t,simdata_X(:,1:N-1),'r', 'LineWidth',2)
% legend('xway','x')
% xlabel('Time (s)')
% ylabel('x(m)')
title('Pose')
grid

figure('Name','system input');
plot(t,simdata_t(:,1:N-1),'b', 'LineWidth',2)
% legend('xway','x')
% xlabel('Time (s)')
% ylabel('x(m)')
title('cable tensions')
grid

t=2:N-1;
figure('Name','Error');
plot(t,Xway(3,2:N-1)-simdata_X(3,2:N-1),'r', 'LineWidth',1)
% legend('xway','x')
% xlabel('Time (s)')
% ylabel('x(m)')
title('error')
grid




