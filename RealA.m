function [A, angle] = RealA(Ai, B, r_p)
%REALA Summary of this function goes here
%   1st to simplify the drawing, we traslate the origin of each pulley
%   frame to the pylley center
%   Here the goal is to use Nguyen method to calcuate the real cable output
%   in pulley
%   Well first B is defined in base frame

 
B = B - Ai;

%%
%Calculate the angles of pulley direction
angle = zeros(8,1);
for i=1:8
    angle(i) = atan2(B(2,i),B(1,i));
end
%Pulley is in (x,z) 

%transform B in pulley plan
for i=1:8
    B(:,i) = Rz(-angle(i))*B(:,i);
end


%%
%Calculate A coordinates
A = nan(3,8);

%Nguyen maths
for i=1:8
    if(i==1 || i==4 || i==5 || i==8)    
        z = (B(3,i) * r_p^2 + r_p * abs(B(1,i) - r_p) * sqrt(B(3,i)^2 +...
            (B(1,i) - r_p)^2 - r_p^2)) / (B(3,i)^2 + (B(1,i) - r_p)^2);
    else
        z = (2*B(3,i) * r_p^2 - r_p * abs(B(1,i) - r_p) * sqrt(B(3,i)^2 +...
            (B(1,i) - r_p)^2 - r_p^2)) / (B(3,i)^2 + (B(1,i) - r_p)^2); 
    end

    A(1,i) = r_p + sqrt(r_p^2 - z^2);

    A(3,i) = z;
    A(2,i) = 0;
end

for i=1:8
    A(:,i) = Rz(angle(i))*A(:,i) + Ai(:,i);
end




end
